# Contacts REST API demo

This is a demo project that allows users to manage list of their contacts

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
This project uses following technological stack:

* [Spring 5]
* [Spring Boot 2]
* [Spring Security]  
* [Liquibase]
* [MapStruct]
* [Springfox / Swagger]

### Prerequisites

* [JDK 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) - Java JDK
* [Maven 3](https://maven.apache.org/download.cgi) - Dependency Management

### Installing

Clone the code of [project repository](https://gitlab.com/nikita.vlasov/contact-api-demo)

```
git clone https://gitlab.com/nikita.vlasov/contact-api-demo.git
```
Compile project using maven

```
mvn clean compile
```

Run project using maven and spring-boot
```
mvn spring-boot:run
```

You can also run the demo with some data already created. Use username: admin and password: admin for authentication
```
mvn spring-boot:run -Dspring-boot.run.profiles=insert-data
```

The server should start and listen on localhost port 9876. 
In order to assign another port, go to [application.properties](src/main/resources/application.properties) and change the value of server.port
```
server.port=8081
```

Try it out with [Postman](https://www.postman.com/downloads/) or with a browser. Use Basic Authentication in Postman.
```
http://localhost:9876/api/v1/contacts
```

Use swagger documentation to learn how to use the REST API
```
http://localhost:9876/swagger-ui/
```

If you want to use an external database, you can modify the [profile](src/main/resources/application-remotedb.yaml).
You will also need to modify existing postgresql dependency to use any java connector for your database provider [pom.xml](pom.xml).
```
mvn spring-boot:run -P-inmemorydb,remotedb
```


## Running the tests

To run the tests use the following maven command

```
mvn clean test
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning

The project uses [GIT](https://git-scm.com/) for versioning. For the versions available, see [GIT downloads](https://git-scm.com/).

## Authors

* **Vlasov Nikita** - *Initial implementation* - [contacts api demo](https://gitlab.com/nikita.vlasov/contact-api-demo)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used

