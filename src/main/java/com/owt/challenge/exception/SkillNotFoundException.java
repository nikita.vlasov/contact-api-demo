package com.owt.challenge.exception;

public class SkillNotFoundException extends RuntimeException{

    private static final String NOT_FOUND = "Skill not found id=";

    public SkillNotFoundException(Long id) {
        super(NOT_FOUND + id);
    }
}
