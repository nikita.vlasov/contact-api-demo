package com.owt.challenge.exception;

public class ContactNotFoundException extends RuntimeException{

    private static final String NOT_FOUND = "Contact not found id=";

    public ContactNotFoundException(Long id) {
        super(NOT_FOUND + id);
    }
}
