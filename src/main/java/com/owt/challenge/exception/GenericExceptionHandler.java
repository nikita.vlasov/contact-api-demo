package com.owt.challenge.exception;

import com.fasterxml.jackson.core.JsonParseException;
import com.owt.challenge.dto.ErrorDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String SEPARATOR = ": ";
    private static final String ACCESS_DENIED = "Access denied!";
    private static final String INVALID_REQUEST = "Invalid request";
    private static final String ERROR_MESSAGE_TEMPLATE = "message: %s %n requested uri: %s";
    private static final String LIST_ELEMENT_DELIMITER = ",";
    private static final String ERRORS_FOR_PATH = "errors %s for path %s";

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException exception, WebRequest request) {
        final List<String> validationErrors = exception.getConstraintViolations().stream().
                map(violation -> violation.getPropertyPath() + SEPARATOR + violation.getMessage())
                .collect(Collectors.toList());
        return getExceptionResponseEntity(exception, HttpStatus.BAD_REQUEST, request, validationErrors);
    }

    @ExceptionHandler({ContactNotFoundException.class, SkillNotFoundException.class})
    public ResponseEntity<Object> handleResourceNotFound(Exception exception, WebRequest request) {
        final HttpStatus status = HttpStatus.NOT_FOUND;
        final String path = request.getDescription(false);
        String message = exception.getMessage();
        log.error(String.format(ERROR_MESSAGE_TEMPLATE, message, path), exception);
        return getExceptionResponseEntity(exception, status, request, Collections.singletonList(message));
    }

    @ExceptionHandler({SkillAlreadyExistException.class})
    public ResponseEntity<Object> handleSkillExist(SkillAlreadyExistException exception, WebRequest request) {
        final HttpStatus status = HttpStatus.CONFLICT;
        final String path = request.getDescription(false);
        String message = exception.getMessage();
        log.error(String.format(ERROR_MESSAGE_TEMPLATE, message, path), exception);
        return getExceptionResponseEntity(exception, status, request, Collections.singletonList(message));
    }

    @ExceptionHandler({UserAlreadyExistException.class})
    public ResponseEntity<Object> handleSkillExist(UserAlreadyExistException exception, WebRequest request) {
        HttpStatus status = exception.getMessage().contains("exist") ? HttpStatus.CONFLICT : HttpStatus.BAD_REQUEST;
        final String path = request.getDescription(false);
        String message = exception.getMessage();
        log.error(String.format(ERROR_MESSAGE_TEMPLATE, message, path), exception);
        return getExceptionResponseEntity(exception, status, request, Collections.singletonList(message));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleGenericException(Exception exception, WebRequest request) {
        ResponseStatus responseStatus = exception.getClass().getAnnotation(ResponseStatus.class);
        final HttpStatus status = responseStatus != null ? responseStatus.value():HttpStatus.INTERNAL_SERVER_ERROR;
        final String path = request.getDescription(false);
        String message = status.getReasonPhrase();
        log.error(String.format(ERROR_MESSAGE_TEMPLATE, message, path), exception);
        return getExceptionResponseEntity(exception, status, request, Collections.singletonList(message));
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return getExceptionResponseEntity(exception, status, request, Collections.singletonList(exception.getMessage()));
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
                HttpRequestMethodNotSupportedException exception,
                HttpHeaders headers,
                HttpStatus status,
                WebRequest request) {
        return getExceptionResponseEntity(exception, status, request, Collections.singletonList(exception.getMessage()));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
                MethodArgumentNotValidException exception,
                HttpHeaders headers,
                HttpStatus status,
                WebRequest request) {
        List<String> validationErrors = exception.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(error -> error.getField() + SEPARATOR + error.getDefaultMessage())
                .collect(Collectors.toList());
        return getExceptionResponseEntity(exception, HttpStatus.BAD_REQUEST, request, validationErrors);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
                HttpMessageNotReadableException exception,
                HttpHeaders headers,
                HttpStatus status,
                WebRequest request) {
        String message;
        if(exception.getCause() instanceof JsonParseException){
            message = exception.getCause().getMessage();
        }else{
            message = exception.getMessage();
        }
        return getExceptionResponseEntity(exception, status, request, Collections.singletonList(message));
    }

    private ResponseEntity<Object> getExceptionResponseEntity(final Exception exception,
                                                              final HttpStatus status,
                                                              final WebRequest request,
                                                              final List<String> errors) {
        final String path = request.getDescription(false);
        ErrorDto errorMessage = new ErrorDto();
        errorMessage.setTimestamp(Instant.now());
        errorMessage.setStatus(status.value());
        errorMessage.setErrors(errors);
        errorMessage.setType(exception.getClass().getSimpleName());
        errorMessage.setPath(path);
        errorMessage.setMessage(getMessageForStatus(status));

        final String errorsMessage = !CollectionUtils.isEmpty(errors) ?
                errors.stream()
                        .filter(StringUtils::hasText)
                        .collect(Collectors.joining(LIST_ELEMENT_DELIMITER))
                : status.getReasonPhrase();
        log.error(String.format(ERRORS_FOR_PATH, errorsMessage, path));
        return new ResponseEntity<>(errorMessage, status);
    }

    private String getMessageForStatus(HttpStatus status) {
        switch (status) {
            case UNAUTHORIZED:
                return ACCESS_DENIED;
            case BAD_REQUEST:
                return INVALID_REQUEST;
            default:
                return status.getReasonPhrase();
        }
    }
}
