package com.owt.challenge.exception;

public class SkillAlreadyExistException extends RuntimeException{

    private static final String ALREADY_EXIST = "Skill already exist id=";

    public SkillAlreadyExistException(Long id) {
        super(ALREADY_EXIST+id);
    }
}
