package com.owt.challenge.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@ApiModel(value = "ErrorInfo", description = "Error model used to give the details about error")
@NoArgsConstructor @Getter @Setter
public class ErrorDto {

        private Instant timestamp;
        private int status;
        private List<String> errors;
        private String type;
        private String path;
        private String message;
}
