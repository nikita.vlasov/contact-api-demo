package com.owt.challenge.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@NoArgsConstructor @Data
@ApiModel(value = "ContactInfo", description = "Contact model used to create or update contact information")
public class ContactIncomingDto {

    @ApiModelProperty(
            value = "First name of the contact",
            required = true,
            example = "Clark")
    @NotBlank(message = "First name should not be null or blank")
    @Size(min = 2, max = 30)
    private String firstName;

    @ApiModelProperty(
            value = "Last name of the contact",
            required = true,
            example = "Kent")
    @NotBlank(message = "Last name should not be null or blank")
    @Size(min = 2, max = 30)
    private String lastName;

    @ApiModelProperty(
            value = "Address of the contact",
            example = "Smallville")
    private String address;

    @ApiModelProperty(
            value = "Email of the contact",
            example = "superman@kripton.net")
    @Email(message = "Email should be valid")
    private String email;

    @ApiModelProperty(
            value = "Mobile number of the contact",
            example = "123 45 67")
    private String mobileNumber;
}
