package com.owt.challenge.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@ApiModel(value = "ContactDetailedInfo", description = "Contact model used to show contact's information")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class ContactDto {

        @ApiModelProperty(
                value = "Contact id",
                required = true,
                example = "0")
        private long id;

        @ApiModelProperty(
                value = "First name of the contact",
                required = true,
                example = "Clark")
        @NotBlank(message = "First name should not be null or blank")
        private String firstName;

        @ApiModelProperty(
                value = "Last name of the contact",
                required = true,
                example = "Kent")
        @NotBlank(message = "Last name should not be null or blank")
        @Size(min = 2, max = 30)
        private String lastName;

        @ApiModelProperty(
                value = "Last name of the contact",
                required = true,
                example = "Clark Kent")
        @Size(min = 5, max = 61)
        @NotBlank(message = "Full name should not be null or blank")
        private String fullName;

        @ApiModelProperty(
                value = "Address of the contact",
                example = "Smallville")
        private String address;

        @ApiModelProperty(
                value = "Email of the contact",
                example = "superman@kripton.net")
        @Email(message = "Email should be valid")
        private String email;

        @ApiModelProperty(
                value = "Mobile number of the contact",
                example = "123 45 67")
        private String mobileNumber;

        @ApiModelProperty(value = "Set of skills of the contact")
        private Set<SkillDto> skills = new HashSet<>();

        @Override
        public String toString() {
                return "ContactDto{" +
                        "id=" + id +
                        ", firstName='" + firstName + '\'' +
                        ", lastName='" + lastName + '\'' +
                        ", fullName='" + fullName + '\'' +
                        ", address='" + address + '\'' +
                        ", email='" + email + '\'' +
                        ", mobileNumber='" + mobileNumber + '\'' +
                        '}';
        }
}
