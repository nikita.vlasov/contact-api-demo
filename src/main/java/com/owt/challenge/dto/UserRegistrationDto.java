package com.owt.challenge.dto;

import com.owt.challenge.validator.RegistrationValidationConstraint;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel(value = "RegistrationInfo", description = "Registration information")
@NoArgsConstructor @Getter @Setter
@RegistrationValidationConstraint
public class UserRegistrationDto {

    @NotNull
    @NotBlank
    private String username;

    @NotNull
    @NotBlank
    private String password;

    @NotNull
    @NotBlank
    private String repeatPassword;
}

