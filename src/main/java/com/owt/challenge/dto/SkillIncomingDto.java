package com.owt.challenge.dto;

import com.owt.challenge.entity.Expertise;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel(value = "SkillInfo", description = "Skill model used to create or update skill information")
@NoArgsConstructor @AllArgsConstructor @Data
public class SkillIncomingDto {

    @ApiModelProperty(
            value = "Skill id",
            required = true,
            example = "0")
    private long id;

    @ApiModelProperty(
            value = "Skill name",
            required = true,
            example = "Java")
    @NotNull
    @NotBlank(message = "Skill name should not be null or blank")
    private String skillName;

    @ApiModelProperty(
            value = "Lever of Expertise",
            notes = "In case, level is not provided, not recognized or null, default value NOT_APPLICABLE is be assigned. " +
                    "Lower case is accepted.",
            example = "NOT_APPLICABLE")
    private Expertise level = Expertise.NOT_APPLICABLE;

    public void setLevel(Expertise level){
        if(level != null){
            this.level = level;
        }
    }
}
