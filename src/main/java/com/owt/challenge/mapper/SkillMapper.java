package com.owt.challenge.mapper;

import com.owt.challenge.dto.SkillDto;
import com.owt.challenge.dto.SkillIncomingDto;
import com.owt.challenge.entity.Skill;
import org.mapstruct.*;

import java.util.Set;

@Mapper(componentModel = "spring", uses = {ContactMapper.class})
public interface SkillMapper {

    Skill toSkill(SkillIncomingDto skillDto);

    @Mapping(target = "contacts", ignore = true)
    SkillDto toSkillDtoWithoutContacts(Skill skill);

    @Named("No contacts")
    @Mappings(@Mapping(target = "contacts", qualifiedByName = "toContactDtoSetIgnoreSkills"))
    SkillDto toSkillDtoIgnoreContacts(Skill skill);

    @Named("toSkillDtoSetIgnoreContacts")
    @IterableMapping(qualifiedByName = "toSkillDtoIgnoreContacts")
    Set<SkillDto> toSkillDtoSetIgnoreContacts(Set<Skill> skill);

    @Named("toSkillDtoIgnoreContacts")
    @Mappings({@Mapping(target = "level", source = "skill.level"),
              @Mapping(target = "contacts", ignore = true)})
    SkillDto toSkillDtoFromSetIgnoreContacts(Skill skill);
}