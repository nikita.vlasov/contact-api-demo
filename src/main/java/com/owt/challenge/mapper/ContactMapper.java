package com.owt.challenge.mapper;

import com.owt.challenge.dto.ContactDto;
import com.owt.challenge.dto.ContactIncomingDto;
import com.owt.challenge.entity.Contact;
import org.mapstruct.*;

import java.util.Set;

@Mapper(componentModel = "spring", uses = {SkillMapper.class})
public interface ContactMapper {

    @Mappings(@Mapping(target = "fullName",
              expression= "java( contactIncomingDto.getFirstName() + \" \" + contactIncomingDto.getLastName())"))
    Contact toContact(ContactIncomingDto contactIncomingDto);

    @Named("Without skills")
    @Mapping(target = "skills", ignore = true)
    ContactDto toContactDtoWithoutSkills(Contact contact);

    @Named("No skills")
    @Mappings(@Mapping(target = "skills", qualifiedByName = "toSkillDtoSetIgnoreContacts"))
    ContactDto toContactDtoIgnoreSkills(Contact contact);

    @Named("toContactDtoSetIgnoreSkills")
    @IterableMapping(qualifiedByName = "toContactDtoIgnoreSkills")
    Set<ContactDto> toContactDtoSetIgnoreSkills(Set<Contact> contact);

    @Named("toContactDtoIgnoreSkills")
    @Mappings(@Mapping(target = "skills", ignore = true))
    ContactDto toContactDtoFromSetIgnoreSkills(Contact contact);
}