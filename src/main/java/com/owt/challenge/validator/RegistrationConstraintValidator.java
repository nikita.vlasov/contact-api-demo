package com.owt.challenge.validator;

import com.owt.challenge.dto.UserRegistrationDto;
import com.owt.challenge.entity.User;
import com.owt.challenge.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@RequiredArgsConstructor
@Component
public class RegistrationConstraintValidator  implements ConstraintValidator<RegistrationValidationConstraint, UserRegistrationDto> {

    private final UserRepository repository;

    @Override
    public boolean isValid(UserRegistrationDto userRegistrationDto, ConstraintValidatorContext context) {
        if(!userRegistrationDto.getPassword().equals(userRegistrationDto.getRepeatPassword())){
            context.buildConstraintViolationWithTemplate("Passwords don't match.")
                    .addPropertyNode("password").addConstraintViolation();
            return false;
        }

        User user = repository.findByUsername(userRegistrationDto.getUsername());
        if(user != null){
            context.buildConstraintViolationWithTemplate("Username already exist.")
                    .addPropertyNode("username").addConstraintViolation();
            return false;
        }
        return true;
    }
}
