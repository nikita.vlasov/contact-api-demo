package com.owt.challenge.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Constraint(validatedBy = RegistrationConstraintValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface RegistrationValidationConstraint {
    String message() default "Registration form should be valid.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
