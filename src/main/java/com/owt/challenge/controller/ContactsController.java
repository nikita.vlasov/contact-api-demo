package com.owt.challenge.controller;

import com.owt.challenge.dto.ContactDto;
import com.owt.challenge.dto.ContactIncomingDto;
import com.owt.challenge.dto.ErrorDto;
import com.owt.challenge.entity.User;
import com.owt.challenge.service.ContactService;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

import static com.owt.challenge.paths.Paths.API;
import static com.owt.challenge.paths.Paths.V1;

@Api(tags = "Contact management endpoint")
@AllArgsConstructor @Slf4j
@RestController
@RequestMapping(API + V1 + "/contacts")
public class ContactsController {

    private final ContactService service;

    @ApiOperation(value = "Find all contacts",
                  notes = "This method retrieves the list of all contacts. Empty list is returned if no contacts found." +
                          "If skillId is present the list will contain all contacts that have the skill identified by skillId")
    @ApiResponses({ @ApiResponse(code = 200, message = "Success") })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    public List<ContactDto> getAll(@AuthenticationPrincipal User user,
                                   @ApiParam(name =  "skillId",type = "Long", value = "Id of a skill.", example = "1")
                                   @RequestParam(required = false) Long skillId){
        return service.findAll(skillId, user);
    }

    @ApiOperation(value = "Find contact by id", notes = "This method retrieves an existing contact")
    @ApiResponses({ @ApiResponse(code = 200, message = "Contact found"),
                    @ApiResponse(code = 404, message = "Contact not found", response = ErrorDto.class) })
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    public ContactDto getById(@AuthenticationPrincipal User user, @PathVariable long id){
        return service.findById(id, user);
    }

    @ApiOperation(value = "Create contact", notes = "This method creates a new contact")
    @ApiResponses({ @ApiResponse(code = 201, message = "Contact created"),
                    @ApiResponse(code = 400, message = "Contact validation failed", response = ErrorDto.class) })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    public ContactDto create(HttpServletResponse response, @AuthenticationPrincipal User user, @RequestBody @Valid ContactIncomingDto contact){
        ContactDto dto = service.create(contact, user);
        response.addHeader("location", API + V1 + "/contacts/" + dto.getId());
        log.info(String.format("New contact %s created by user %s",dto, user));
        return dto;
    }

    @ApiOperation(value = "Update contact", notes = "This method updates an existing contact")
    @ApiResponses({ @ApiResponse(code = 200, message = "Contact updated"),
                    @ApiResponse(code = 400, message = "Contact validation failed", response = ErrorDto.class),
                    @ApiResponse(code = 404, message = "Contact not found", response = ErrorDto.class) })
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    public ContactDto update(HttpServletResponse response, @AuthenticationPrincipal User user, @PathVariable long id, @RequestBody @Valid ContactIncomingDto contact){
        ContactDto dto = service.update(id, contact, user);
        response.addHeader("location", API + V1 + "/contacts/" + dto.getId());
        log.info(String.format("Contact %s updated by user %s",dto, user));
        return dto;
    }

    @ApiOperation(value = "Delete contact", notes = "This method deletes an existing contact")
    @ApiResponses({
                    @ApiResponse(code = 204, message = "Contact deleted", response = void.class),
                    @ApiResponse(code = 404, message = "Contact not found", response = ErrorDto.class)
                    })
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    public void delete(@AuthenticationPrincipal User user, @PathVariable long id){
        service.delete(id, user);
        log.info(String.format("Contact %d deleted by user %s",id, user));
    }
}
