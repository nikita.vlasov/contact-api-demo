package com.owt.challenge.controller;

import com.owt.challenge.dto.ErrorDto;
import com.owt.challenge.dto.UserRegistrationDto;
import com.owt.challenge.entity.Role;
import com.owt.challenge.entity.User;
import com.owt.challenge.exception.UserAlreadyExistException;
import com.owt.challenge.repository.UserRepository;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RequiredArgsConstructor
@Controller
@RequestMapping(path = "/registration")
public class RegistrationController {

    private final UserRepository repository;
    private final PasswordEncoder encoder;

    @GetMapping
    public String getView(@ModelAttribute("registration") UserRegistrationDto registration){
        registration.setPassword("");
        registration.setRepeatPassword("");
        registration.setUsername("");
        return "registration";
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponses({
            @ApiResponse(code = 201, message = "User created", response = void.class),
            @ApiResponse(code = 409, message = "User already exist", response = ErrorDto.class),
            @ApiResponse(code = 400, message = "Validation error", response = ErrorDto.class)
    })
    public void registerFromJSON(@RequestBody @Valid UserRegistrationDto registration, BindingResult validationResult){
        if(!validationResult.hasErrors()){
            createUser(registration);
        }else if(validationResult.hasFieldErrors("username")){
            throw new UserAlreadyExistException("Username already exist");
        }else{
            throw new UserAlreadyExistException("Passwords don't match.");
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String registerFromForm(@ModelAttribute("registration") @Valid UserRegistrationDto registration, BindingResult validationResult){
        if(validationResult.hasErrors())
            return "registration";

        createUser(registration);
        return "redirect:/swagger-ui/";
    }

    private void createUser(UserRegistrationDto registration) {
        final User user = new User();
        user.setUsername(registration.getUsername());
        user.setPassword(encoder.encode(registration.getPassword()));
        user.setRoles(Set.of(Role.USER));
        user.setIsActive(true);
        repository.save(user);
    }
}
