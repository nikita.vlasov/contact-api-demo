package com.owt.challenge.controller;

import com.owt.challenge.dto.ErrorDto;
import com.owt.challenge.dto.SkillDto;
import com.owt.challenge.dto.SkillIncomingDto;
import com.owt.challenge.entity.User;
import com.owt.challenge.service.SkillService;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

import static com.owt.challenge.paths.Paths.API;
import static com.owt.challenge.paths.Paths.V1;

@Api(tags = "Skills management endpoint")
@AllArgsConstructor @Slf4j
@RestController
@RequestMapping(API + V1 + "/skills")
public class SkillsController {

    private final SkillService service;

    @ApiOperation(value = "Find all skills",
            notes = "This method retrieves the list of all skills. Empty list is returned if no skills found." +
                    "If contactId is present the list will contain all skills that have the contact identified by contactId")
    @ApiResponses({ @ApiResponse(code = 200, message = "Success") })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<SkillDto> getAll(@AuthenticationPrincipal User user,
                                 @ApiParam(name =  "contactId",type = "Long", value = "Id of a contact.", example = "1")
                                 @RequestParam(required = false) Long contactId){
        return service.findAll(contactId, user);
    }

    @ApiOperation(value = "Find skill by id", notes = "This method retrieves an existing skill")
    @ApiResponses({ @ApiResponse(code = 200, message = "Skill found"),
                    @ApiResponse(code = 404, message = "Skill not found", response = ErrorDto.class) })
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public SkillDto getById(@AuthenticationPrincipal User user,
                            @PathVariable long id){
        return service.findById(id, user);
    }

    @ApiOperation(value = "Create skill",
                  notes = "This method creates a new skill. " +
                    "In case, level is not provided, not recognized or null, default value NOT_APPLICABLE will be assigned.")
    @ApiResponses({ @ApiResponse(code = 201, message = "Skill created"),
                    @ApiResponse(code = 400, message = "Skill validation failed", response = ErrorDto.class),
                    @ApiResponse(code = 409, message = "Skill with this name and level already exist", response = ErrorDto.class) })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public SkillDto create(HttpServletResponse response, @AuthenticationPrincipal User user,
                           @RequestBody @Valid SkillIncomingDto skillDto){
        SkillDto dto = service.create(skillDto, user);
        response.addHeader("location", API + V1 + "/skills/" + dto.getId());
        log.info(String.format("New Skill %s created by user %s",dto, user));
        return dto;
    }

    @ApiOperation(value = "Update skill",
                  notes = "This method updates an existing skill. " +
                  "In case, level is not provided, not recognized or null, default value NOT_APPLICABLE will be assigned.")
    @ApiResponses({ @ApiResponse(code = 200, message = "Skill updated"),
                    @ApiResponse(code = 400, message = "Skill validation failed", response = ErrorDto.class),
                    @ApiResponse(code = 404, message = "Skill not found", response = ErrorDto.class),
                    @ApiResponse(code = 409, message = "Skill  with this already exist", response = ErrorDto.class)  })
    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public SkillDto update(HttpServletResponse response, @AuthenticationPrincipal User user,
                           @PathVariable long id, @RequestBody @Valid SkillIncomingDto skillDto){
        SkillDto dto = service.update(id, skillDto, user);
        response.addHeader("location", API + V1 + "/skills/" + dto.getId());
        log.info(String.format("Skill %s updated by user %s",dto , user));
        return dto;
    }

    @ApiOperation(value = "Add skill to contact",
                  notes = "This method adds skill to contact's list of skills, if skill is not present in contact's list of skills.")
    @ApiResponses({ @ApiResponse(code = 200, message = "Skill added to contact's list", response = void.class),
                    @ApiResponse(code = 404, message = "Skill or contact not found", response = ErrorDto.class) })
    @PutMapping(path = "/{skillId}/contact/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    public void addSkillToContact(@AuthenticationPrincipal User user, @PathVariable long skillId, @PathVariable long contactId){
        service.addSkillToContact(skillId, contactId, user);
        log.info(String.format("Skill %d added to contact %d by user %s",skillId, contactId, user));
    }

    @ApiOperation(value = "Delete skill", notes = "This method deletes an existing skill")
    @ApiResponses({ @ApiResponse(code = 204, message = "Skill deleted", response = void.class),
                    @ApiResponse(code = 404, message = "Skill not found", response = ErrorDto.class) })
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@AuthenticationPrincipal User user, @PathVariable long id){
        service.delete(id, user);
        log.info(String.format("Skill %d removed by user %s",id, user));
    }

    @ApiOperation(value = "Remove skill from contact",
            notes = "This method removes skill from contact's list of skills, if skill is present in contact's list of skills.")
    @ApiResponses({ @ApiResponse(code = 200, message = "Skill removed from contact's list", response = void.class),
                    @ApiResponse(code = 404, message = "Skill or contact not found", response = ErrorDto.class) })
    @DeleteMapping(path = "/{skillId}/contact/{contactId}")
    @ResponseStatus(HttpStatus.OK)
    public void removeSkillFromContact(@AuthenticationPrincipal User user, @PathVariable long skillId,@PathVariable long contactId){
        service.removeSkillFromContact(skillId, contactId,user);
        log.info(String.format("Skill %d added to contact %d by user %s",skillId, contactId, user));
    }
}
