package com.owt.challenge.service;

import com.owt.challenge.dto.SkillDto;
import com.owt.challenge.dto.SkillIncomingDto;
import com.owt.challenge.entity.Contact;
import com.owt.challenge.entity.Skill;
import com.owt.challenge.entity.User;
import com.owt.challenge.exception.ContactNotFoundException;
import com.owt.challenge.exception.SkillAlreadyExistException;
import com.owt.challenge.exception.SkillNotFoundException;
import com.owt.challenge.mapper.SkillMapper;
import com.owt.challenge.repository.ContactRepository;
import com.owt.challenge.repository.SkillRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
@Validated
public class SkillService {

    private final SkillRepository repository;
    private final ContactRepository contactRepository;
    private final SkillMapper skillMapper;

    public SkillDto findById(long id, User user) throws SkillNotFoundException {
        Skill skill = repository.findById(id).orElseThrow(() -> new SkillNotFoundException(id));
        return skillMapper.toSkillDtoIgnoreContacts(skill);
    }

    public List<SkillDto> findAll(Long contactId, User user){
        List<SkillDto> skills = new ArrayList<>();
        if(contactId!=null){
            repository.findAllByContacts_idAndOwner_id(contactId, user.getId()).forEach(skill -> skills.add(skillMapper.toSkillDtoWithoutContacts(skill)));
        }else {
            repository.findAllByOwner_idOrderByIdAsc(user.getId()).forEach(skill -> skills.add(skillMapper.toSkillDtoIgnoreContacts(skill)));
        }
        return skills;
    }

    public SkillDto create(@Valid SkillIncomingDto skillDto, User user){
        Skill skill = skillMapper.toSkill(skillDto);
        checkExistenceOrThrowException(skill, user);
        Skill savedSkill = repository.save(skill);
        return skillMapper.toSkillDtoIgnoreContacts(savedSkill);
    }

    public SkillDto update(long id, @Valid SkillIncomingDto skillDto, User user) throws SkillNotFoundException {
        Skill toUpdate = repository.findByIdAndOwner_id(id, user.getId()).orElseThrow(() -> new SkillNotFoundException(id));
        Skill skill = skillMapper.toSkill(skillDto);
        checkExistenceOrThrowException(skill, user);
        skill.setId(toUpdate.getId());
        repository.save(skill);
        return skillMapper.toSkillDtoIgnoreContacts(skill);
    }

    public void delete(long id, User user) {
        repository.findByIdAndOwner_id(id, user.getId()).orElseThrow(() -> new SkillNotFoundException(id));
        repository.deleteById(id);
    }

    public void addSkillToContact(long skillId, long contactId, User user) {
        Skill skill = repository.findById(skillId).orElseThrow(() -> new SkillNotFoundException(skillId));
        Contact contact = contactRepository.findById(contactId).orElseThrow(() -> new ContactNotFoundException(contactId));
        if(!skill.getContacts().contains(contact)){
            skill.getContacts().add(contact);
            repository.save(skill);
        }
    }

    public void removeSkillFromContact(long skillId, long contactId, User user) {
        Skill skill = repository.findById(skillId).orElseThrow(() -> new SkillNotFoundException(skillId));
        Contact contact = contactRepository.findById(contactId).orElseThrow(() -> new ContactNotFoundException(contactId));
        if(skill.getContacts().contains(contact)){
            skill.getContacts().remove(contact);
            repository.save(skill);
        }
    }

    private void checkExistenceOrThrowException(Skill skill, User user) {
        Optional<Skill> entity = repository.findBySkillNameAndLevelAndOwner_id(skill.getSkillName(), skill.getLevel(), user.getId());
        if(entity.isPresent()) throw new SkillAlreadyExistException(entity.get().getId());
    }
}
