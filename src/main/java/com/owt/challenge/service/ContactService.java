package com.owt.challenge.service;

import com.owt.challenge.dto.ContactDto;
import com.owt.challenge.dto.ContactIncomingDto;
import com.owt.challenge.entity.Contact;
import com.owt.challenge.entity.User;
import com.owt.challenge.exception.ContactNotFoundException;
import com.owt.challenge.mapper.ContactMapper;
import com.owt.challenge.repository.ContactRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
@Validated
public class ContactService {

    private final ContactRepository repository;
    private final ContactMapper contactMapper;

    public ContactDto findById(long id, User user) throws ContactNotFoundException {
        Contact contact = repository.findByIdAndOwner_id(id, user.getId()).orElseThrow(() -> new ContactNotFoundException(id));
        return contactMapper.toContactDtoIgnoreSkills(contact);
    }

    public List<ContactDto> findAll(Long skillId, User user){
        List<ContactDto> contactList = new ArrayList<>();
        if(skillId != null) {
            repository.findBySkills_idAndOwner_id(skillId, user.getId()).forEach(contact -> contactList.add(contactMapper.toContactDtoWithoutSkills(contact)));
        }else {
            repository.findAllByOwner_idOrderByIdAsc(user.getId()).forEach(contact -> contactList.add(contactMapper.toContactDtoIgnoreSkills(contact)));
        }
        return contactList;
    }

    public ContactDto create(@Valid ContactIncomingDto contactIncomingDto, User user){
        Contact contact = contactMapper.toContact(contactIncomingDto);
        contact.setOwner(user);
        repository.save(contact);
        return contactMapper.toContactDtoIgnoreSkills(contact);
    }

    public ContactDto update(long id, @Valid ContactIncomingDto contactIncomingDto, User user) throws ContactNotFoundException {
        Contact toUpdate = repository.findByIdAndOwner_id(id, user.getId()).orElseThrow(() -> new ContactNotFoundException(id));
        Contact contact = contactMapper.toContact(contactIncomingDto);
        contact.setId(toUpdate.getId());
        repository.save(contact);
        return contactMapper.toContactDtoFromSetIgnoreSkills(contact);
    }

    public void delete(long id, User user) {
        repository.findByIdAndOwner_id(id, user.getId()).orElseThrow(() -> new ContactNotFoundException(id));
        repository.deleteById(id);
    }
}
