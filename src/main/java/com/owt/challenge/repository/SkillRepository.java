package com.owt.challenge.repository;

import com.owt.challenge.entity.Expertise;
import com.owt.challenge.entity.Skill;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import java.util.Optional;

@Repository
@Validated
public interface SkillRepository extends CrudRepository<Skill, Long> {
    Optional<Skill> findBySkillNameAndLevelAndOwner_id(String skillName, Expertise level, Long ownerId);
    Optional<Skill> findByIdAndOwner_id(long id, Long ownerId);
    Iterable<Skill> findAllByOwner_idOrderByIdAsc(Long ownerId);
    Iterable<Skill> findAllByContacts_idAndOwner_id(Long id, Long ownerId);
}
