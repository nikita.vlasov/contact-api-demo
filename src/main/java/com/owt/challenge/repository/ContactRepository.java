package com.owt.challenge.repository;

import com.owt.challenge.entity.Contact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import java.util.Optional;

@Repository
@Validated
public interface ContactRepository extends CrudRepository<Contact, Long> {
    Iterable<Contact> findAllByOwner_idOrderByIdAsc(Long id);
    Iterable<Contact> findBySkills_idAndOwner_id(Long id, Long ownerId);
    Optional<Contact> findByIdAndOwner_id(Long id, Long ownerId);
}
