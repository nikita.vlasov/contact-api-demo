package com.owt.challenge.configuration;

import com.owt.challenge.entity.Contact;
import com.owt.challenge.entity.Skill;
import com.owt.challenge.entity.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

import static com.owt.challenge.paths.Paths.API;
import static com.owt.challenge.paths.Paths.V1;
import static springfox.documentation.service.ApiInfo.DEFAULT_CONTACT;

@Configuration
@PropertySource("classpath:properties.yaml")
@ConfigurationProperties(prefix = "app")
public class SwaggerConfig {

    @Value("${doc-version}")
    private String version;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .directModelSubstitute(void.class, java.lang.Void.class)
                .ignoredParameterTypes(Contact.class, Skill.class, User.class)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.owt.challenge"))
                .paths(PathSelectors.ant(API + V1 + "/**")
                        .or(PathSelectors.ant("/registration")))
                .build()
                .securityContexts(Collections.singletonList(apiSecurityContext()))
                .securitySchemes(Collections.singletonList(basicAuthScheme()))
                .apiInfo(apiInfo());
    }

    private SecurityContext apiSecurityContext() {
        return SecurityContext.builder()
                .securityReferences(Collections.singletonList(new SecurityReference("basicAuth", new AuthorizationScope[0])))
                .build();
    }

    private SecurityScheme basicAuthScheme() {
        return new BasicAuth("basicAuth");
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Contacts REST API",
                "Contacts API allows users to manage list of contacts.",
                version,
                "urn:tos",
                DEFAULT_CONTACT,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                Collections.emptyList());
    }
}
