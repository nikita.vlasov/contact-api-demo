package com.owt.challenge.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor @AllArgsConstructor @Getter @Setter
@Entity
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"skill_name", "level"}))
public class Skill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @NotBlank(message = "Skill name should not be null or blank")
    @Column(name = "skill_name")
    private String skillName;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(name = "level")
    private Expertise level = Expertise.NOT_APPLICABLE;

    @ManyToMany
    private Set<Contact> contacts = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    private User owner;
}
