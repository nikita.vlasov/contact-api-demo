package com.owt.challenge.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor @AllArgsConstructor @Getter @Setter
@Entity
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @NotBlank(message = "First name should not be null or blank")
    @Size(min = 2, max = 30)
    @Column(nullable = false,length = 30)
    private String firstName;

    @NotNull
    @NotBlank(message = "Last name should not be null or blank")
    @Size(min = 2, max = 30)
    @Column(nullable = false,length = 30)
    private String lastName;

    @NotBlank(message = "Full name should not be null or blank")
    @Size(min = 5, max = 61)
    @Column(nullable = false,length = 61)
    private String fullName;

    private String address;

    @Email(message = "Email should be valid")
    private String email;

    private String mobileNumber;

    @ManyToMany(mappedBy = "contacts")
    private Set<Skill> skills = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    private User owner;
}
