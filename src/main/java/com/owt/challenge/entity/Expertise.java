package com.owt.challenge.entity;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.stream.Stream;

public enum Expertise {
    NOT_APPLICABLE,
    FUNDAMENTAL_AWARENESS,
    NOVICE,
    INTERMEDIATE,
    ADVANCED,
    EXPERT;

    @JsonCreator
    public static Expertise decode(final String value) {
        return Stream.of(Expertise.values()).filter(
                targetEnum -> targetEnum.name().equals(value.toUpperCase())).findFirst().orElse(NOT_APPLICABLE);
    }
}
