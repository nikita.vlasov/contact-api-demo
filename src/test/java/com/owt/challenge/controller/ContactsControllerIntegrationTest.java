package com.owt.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.owt.challenge.ChallengeApplication;
import com.owt.challenge.dto.ContactDto;
import com.owt.challenge.dto.ContactIncomingDto;
import com.owt.challenge.entity.Contact;
import com.owt.challenge.mapper.ContactMapper;
import com.owt.challenge.repository.ContactRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

import static com.owt.challenge.paths.Paths.API;
import static com.owt.challenge.paths.Paths.V1;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ChallengeApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles({"liquibase-test", "inmemorydb"})
class ContactsControllerIntegrationTest {

    @Autowired
    private ContactRepository contactRepo;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private ContactMapper contactMapper;

    @Test
    @Transactional
    void shouldReturnList_whenContactsExist() throws Exception {
        Set<Contact> existingContacts = new HashSet<>();
        contactRepo.findAll().forEach(existingContacts::add);

        MvcResult result = mvc.perform(get(API + V1 + "/contacts")
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(
                mapper.writeValueAsString(
                        contactMapper.toContactDtoSetIgnoreSkills(existingContacts)
                                .stream().sorted(Comparator.comparing(ContactDto::getId)).collect(Collectors.toList())));
    }

    @Test
    @Transactional
    void shouldReturnContact_whenContactExists() throws Exception {
        Optional<Contact> entity = contactRepo.findById(3L);

        assertThat(entity.isPresent()).isTrue();

        Contact contact = entity.get();

        MvcResult result = mvc.perform(get(API + V1 + "/contacts/3")
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString())
                .isEqualToIgnoringWhitespace(mapper.writeValueAsString(
                        contactMapper.toContactDtoIgnoreSkills(contact)));
    }

    @Test
    void shouldCreateContact_whenIncomingDtoIsValid() throws Exception {
        ContactIncomingDto incomingDto = new ContactIncomingDto();
        incomingDto.setFirstName("FirstName");
        incomingDto.setLastName("LastName");

        MvcResult result = mvc.perform(post(API + V1 + "/contacts")
                .header("Authorization", "Basic YWRtaW46YWRtaW4=")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(incomingDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        ContactDto savedContact = mapper.readValue(result.getResponse().getContentAsString(), ContactDto.class);

        assertThat(savedContact.getId()).isNotNull();

        Optional<Contact> entity = contactRepo.findById(savedContact.getId());

        assertThat(entity.isPresent()).isTrue();

        Contact createdContact = entity.get();

        assertThat(createdContact.getFirstName()).isEqualTo(incomingDto.getFirstName());
        assertThat(createdContact.getLastName()).isEqualTo(incomingDto.getLastName());
        assertThat(createdContact.getFullName()).isEqualTo(incomingDto.getFirstName() + " " + incomingDto.getLastName());
    }

    @Test
    void shouldUpdateContact_whenContactExistsAndIncomingDtoIsValid() throws Exception {
        ContactIncomingDto incomingDto = new ContactIncomingDto();
        incomingDto.setFirstName("UpdatedFirstName");
        incomingDto.setLastName("UpdatedLastName");
        incomingDto.setEmail("new@email.com");

        MvcResult result = mvc.perform(put(API + V1 + "/contacts/2")
                .header("Authorization", "Basic YWRtaW46YWRtaW4=")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(incomingDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ContactDto savedContact = mapper.readValue(result.getResponse().getContentAsString(), ContactDto.class);

        assertThat(savedContact.getId()).isEqualTo(2L);

        Optional<Contact> entity = contactRepo.findById(2L);

        assertThat(entity.isPresent()).isTrue();

        Contact createdContact = entity.get();

        assertThat(createdContact.getFirstName()).isEqualTo(incomingDto.getFirstName());
        assertThat(createdContact.getLastName()).isEqualTo(incomingDto.getLastName());
        assertThat(createdContact.getFullName()).isEqualTo(incomingDto.getFirstName() + " " + incomingDto.getLastName());
        assertThat(createdContact.getEmail()).isEqualTo(incomingDto.getEmail());
    }

    @Test
    void shouldDeleteContact_whenContactExists() throws Exception {
        mvc.perform(delete(API + V1 + "/contacts/1")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isNoContent());

        Optional<Contact> entity = contactRepo.findById(1L);

        assertThat(entity.isPresent()).isFalse();
    }
}