package com.owt.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.owt.challenge.ChallengeApplication;
import com.owt.challenge.dto.ContactDto;
import com.owt.challenge.dto.ContactIncomingDto;
import com.owt.challenge.dto.SkillDto;
import com.owt.challenge.entity.Contact;
import com.owt.challenge.entity.Expertise;
import com.owt.challenge.entity.Skill;
import com.owt.challenge.mapper.SkillMapper;
import com.owt.challenge.repository.ContactRepository;
import com.owt.challenge.repository.SkillRepository;
import liquibase.pro.packaged.S;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;
import java.util.*;

import static com.owt.challenge.paths.Paths.API;
import static com.owt.challenge.paths.Paths.V1;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ChallengeApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles({"liquibase-test", "inmemorydb"})
class SkillsControllerIntegrationTest {

    @Autowired
    private SkillRepository skillRepo;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private SkillMapper skillMapper;

    @Test
    @Transactional
    void shouldReturnList_whenSkillExist() throws Exception {
        Set<Skill> existingSkills = new HashSet<>();
        skillRepo.findAllByOwner_idOrderByIdAsc(1L).forEach(existingSkills::add);

        Set<SkillDto> skillDtos = skillMapper.toSkillDtoSetIgnoreContacts(existingSkills);
        MvcResult result = mvc.perform(get(API + V1 + "/skills")
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk())
                .andReturn();

        List<SkillDto> resultDto = mapper.readerForListOf(SkillDto.class).readValue(result.getResponse().getContentAsString());

        assertThat(resultDto.containsAll(skillDtos)).isTrue();
        assertThat(resultDto.size()).isEqualTo(existingSkills.size());
    }

    @Test
    @Transactional
    void shouldReturnEmptyList_whenSkillExistOnlyForAnotherUser() throws Exception {
        Set<Skill> existingSkills = new HashSet<>();
        skillRepo.findAllByOwner_idOrderByIdAsc(1L).forEach(existingSkills::add);

        Set<SkillDto> skillDtos = skillMapper.toSkillDtoSetIgnoreContacts(existingSkills);
        MvcResult result = mvc.perform(get(API + V1 + "/skills")
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Basic dXNlcjp1c2Vy"))
                .andExpect(status().isOk())
                .andReturn();

        List<SkillDto> resultDto = mapper.readerForListOf(SkillDto.class).readValue(result.getResponse().getContentAsString());

        assertThat(resultDto.isEmpty()).isTrue();
    }

    @Test
    @Transactional
    void shouldReturnSkillDto_whenSkillExists() throws Exception {
        Optional<Skill> entity = skillRepo.findById(3L);

        assertThat(entity.isPresent()).isTrue();

        Skill skill = entity.get();
        SkillDto skillDto = skillMapper.toSkillDtoIgnoreContacts(skill);
        MvcResult result = mvc.perform(get(API + V1 + "/skills/3")
                .header("Authorization", "Basic YWRtaW46YWRtaW4=")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(skillDto));
    }

    @Test
    void shouldCreateSkill_whenSkillDtoIsValid() throws Exception {
        SkillDto defaultSkill = new SkillDto();
        defaultSkill.setSkillName("Painting");
        defaultSkill.setLevel(Expertise.ADVANCED);

        MvcResult result = mvc.perform(post(API + V1 + "/skills")
                .header("Authorization", "Basic YWRtaW46YWRtaW4=")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(defaultSkill))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        SkillDto savedSkill = mapper.readValue(result.getResponse().getContentAsString(), SkillDto.class);

        assertThat(savedSkill.getId()).isNotNull();

        Optional<Skill> entity = skillRepo.findById(savedSkill.getId());

        assertThat(entity.isPresent()).isTrue();

        Skill createdSkill = entity.get();

        assertThat(createdSkill.getSkillName()).isEqualTo(defaultSkill.getSkillName());
        assertThat(createdSkill.getLevel()).isEqualTo(defaultSkill.getLevel());
    }

    @Test
    void shouldNotCreateSkill_whenSkillAlreadyExist() throws Exception {
        SkillDto defaultSkill = new SkillDto();
        defaultSkill.setSkillName("Existing");

        mvc.perform(post(API + V1 + "/skills")
                .header("Authorization", "Basic YWRtaW46YWRtaW4=")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(defaultSkill))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    void shouldUpdateSkill_whenSkillExistsAndSkillDtoIsValid() throws Exception {
        SkillDto skillDto = new SkillDto();
        skillDto.setSkillName("Dreaming"); //stored value Painting
        skillDto.setLevel(Expertise.ADVANCED); //store value NOT_APPLICABLE

        MvcResult result = mvc.perform(put(API + V1 + "/skills/2")
                .header("Authorization", "Basic YWRtaW46YWRtaW4=")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(skillDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        SkillDto savedSkill = mapper.readValue(result.getResponse().getContentAsString(), SkillDto.class);

        assertThat(savedSkill.getId()).isEqualTo(2L);

        Optional<Skill> entity = skillRepo.findById(2L);

        assertThat(entity.isPresent()).isTrue();

        Skill updatedSkill = entity.get();

        assertThat(updatedSkill.getSkillName()).isEqualTo(skillDto.getSkillName());
        assertThat(updatedSkill.getLevel()).isEqualTo(skillDto.getLevel());
    }

    @Test
    void shouldNotUpdateSkill_whenAnotherSkillAlreadyExist() throws Exception {
        SkillDto defaultSkill = new SkillDto();
        defaultSkill.setSkillName("Existing");

        mvc.perform(put(API + V1 + "/skills/2")
                .header("Authorization", "Basic YWRtaW46YWRtaW4=")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(defaultSkill))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    void shouldDeleteSkill_whenSkillExists() throws Exception {
        mvc.perform(delete(API + V1 + "/skills/1")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isNoContent());

        Optional<Skill> entity = skillRepo.findById(1L);

        assertThat(entity.isPresent()).isFalse();
    }

    @Test
    void shouldAddSkillToContact_whenSkillIsNotInContactList() throws Exception {

        //Add Skill
        mvc.perform(put(API + V1 + "/skills/3/contact/3")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk());

        MvcResult skillResult = mvc.perform(get(API + V1 + "/skills/3")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk())
                .andReturn();

        SkillDto skillDto = mapper.readValue(skillResult.getResponse().getContentAsString(), SkillDto.class );

        assertThat(skillDto.getContacts().size()).isEqualTo(1);
        assertThat(skillDto.getContacts().stream().findFirst().isPresent()).isTrue();
        assertThat(skillDto.getContacts().stream().findFirst().get().getId()).isEqualTo(3L);

        MvcResult contactResult = mvc.perform(get(API + V1 + "/contacts/3")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk())
                .andReturn();

        ContactDto contactDto = mapper.readValue(contactResult.getResponse().getContentAsString(), ContactDto.class );

        assertThat(contactDto.getSkills().size()).isEqualTo(1);
        assertThat(contactDto.getSkills().stream().findFirst().isPresent()).isTrue();
        assertThat(contactDto.getSkills().stream().findFirst().get().getId()).isEqualTo(3L);


        //Remove Skill
        mvc.perform(delete(API + V1 + "/skills/3/contact/3")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk());

        skillResult = mvc.perform(get(API + V1 + "/skills/3")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk())
                .andReturn();

        skillDto = mapper.readValue(skillResult.getResponse().getContentAsString(), SkillDto.class );

        assertThat(skillDto.getContacts().size()).isEqualTo(0);

        contactResult = mvc.perform(get(API + V1 + "/contacts/3")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk())
                .andReturn();

        contactDto = mapper.readValue(contactResult.getResponse().getContentAsString(), ContactDto.class );

        assertThat(contactDto.getSkills().size()).isEqualTo(0);
    }

    @Test
    void shouldReturnError_whenSkillIsNotFound() throws Exception {
        mvc.perform(put(API + V1 + "/skills/100/contact/100")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldReturnError_whenContactNotFound() throws Exception {
        mvc.perform(put(API + V1 + "/skills/3/contact/100")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldReturnOk_whenRemoveAndSkillIsNotAddedToContact() throws Exception {
        mvc.perform(delete(API + V1 + "/skills/3/contact/3")
                .header("Authorization", "Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk());
    }
}