package com.owt.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.owt.challenge.dto.SkillDto;
import com.owt.challenge.dto.SkillIncomingDto;
import com.owt.challenge.entity.Expertise;
import com.owt.challenge.exception.SkillNotFoundException;
import com.owt.challenge.service.SkillService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;

import static com.owt.challenge.paths.Paths.API;
import static com.owt.challenge.paths.Paths.V1;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SkillsController.class)
class SkillsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SkillService service;

    @Autowired
    private ObjectMapper mapper;


    /* Testing getAll() */
    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnEmptyList_whenGetAllNoSkillsFound() throws Exception  {

        List<SkillDto> skills = new ArrayList<>();
        when(service.findAll(null, null)).thenReturn(skills);

        MvcResult result = mvc.perform(get(API + V1 + "/skills")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(skills));
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnListSkills_whenGetAndSkillsFound() throws Exception  {

        SkillDto defaultSkill = new SkillDto();
        defaultSkill.setId(2L);
        defaultSkill.setSkillName("Painting");

        List<SkillDto> contacts = Arrays.asList(
                new SkillDto(1L,"Java", Expertise.EXPERT,new HashSet<>()),
                defaultSkill,
                new SkillDto(3L,"Time traveling", Expertise.FUNDAMENTAL_AWARENESS, new HashSet<>())
        );

        when(service.findAll(null, null)).thenReturn(contacts);

        MvcResult result = mvc.perform(get(API + V1 + "/skills")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(contacts));
    }


    /* Testing getById() */
    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnSkill_whenGetAndIdIsCorrect() throws Exception  {

        SkillDto skillDto = new SkillDto();
        skillDto.setId(1L);
        skillDto.setSkillName("Java");

        when(service.findById(1L, null)).thenReturn(skillDto);

        MvcResult result = mvc.perform(get(API + V1 + "/skills/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(skillDto));
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnNotFound_whenGetAndIdIsWrong() throws Exception  {

        when(service.findById(1L, null)).thenThrow(new SkillNotFoundException(1L));

        MvcResult result = mvc.perform(get(API + V1 + "/skills/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":404");
        assertThat(response).contains("\"errors\":[\"Skill not found id=1\"]");
        assertThat(response).contains("\"type\":\"SkillNotFoundException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/skills/1\"");
        assertThat(response).contains("\"message\":\"Not Found\"");
    }


    /* Testing create() */
    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnBadRequest_whenCreateAndSkillDtoNotValid() throws Exception  {

        SkillDto skillDto = new SkillDto();
        skillDto.setId(1L);

        MvcResult result = mvc.perform(post(API + V1 + "/skills")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(skillDto)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":400");
        assertThat(response).contains("\"type\":\"MethodArgumentNotValidException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/skills\"");
        assertThat(response).contains("\"message\":\"Invalid request\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnUnsupportedMediaType_whenCreateAndContentTypeHeaderIsNotJSON() throws Exception  {

        SkillDto skillDto = new SkillDto();
        skillDto.setId(1L);
        skillDto.setSkillName("Java");

        mvc.perform(post(API + V1 + "/skills")
                .content(mapper.writeValueAsString(skillDto)))
                .andExpect(status().isUnsupportedMediaType());
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnMethodNotAllowed_whenCreateURIIsWrong() throws Exception  {

        SkillDto skillDto = new SkillDto();
        skillDto.setId(1L);
        skillDto.setSkillName("Java");

        MvcResult result = mvc.perform(post(API + V1 + "/skills/1")
                .content(mapper.writeValueAsString(skillDto)))
                .andExpect(status().isMethodNotAllowed())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":405");
        assertThat(response).contains("\"errors\":[\"Request method 'POST' not supported\"]");
        assertThat(response).contains("\"type\":\"HttpRequestMethodNotSupportedException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/skills/1\"");
        assertThat(response).contains("\"message\":\"Method Not Allowed\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldCreateSkill_whenCreateAndSkillDtoIsValid() throws Exception  {

        SkillIncomingDto skillInputDto = new SkillIncomingDto();
        skillInputDto.setSkillName("Java");

        SkillDto skillOutputDto = new SkillDto();
        skillOutputDto.setId(1);
        skillOutputDto.setSkillName(skillInputDto.getSkillName());
        skillOutputDto.setLevel(skillInputDto.getLevel());

        when(service.create(skillInputDto, null)).thenReturn(skillOutputDto);

        MvcResult result = mvc.perform(post(API + V1 + "/skills")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(skillInputDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(skillOutputDto));
    }


    /* Testing update() */
    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnUnsupportedMediaType_whenUpdateAndContentTypeHeaderIsNotJSON() throws Exception  {

        SkillDto skillDto = new SkillDto();
        skillDto.setSkillName("Java");

        mvc.perform(put(API + V1 + "/skills/1")
                .content(mapper.writeValueAsString(skillDto)))
                .andExpect(status().isUnsupportedMediaType());
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnUpdateWithDefaultLevelValue_whenUpdateSkillDtoHasNoValueProvided() throws Exception  {

        SkillIncomingDto skillDto = new SkillIncomingDto();
        skillDto.setSkillName("Java");
        String content = mapper.writeValueAsString(skillDto)
                .replace("\"level\":\"" + Expertise.NOT_APPLICABLE.name() + "\",", "");

        SkillDto skillDtoExpected = new SkillDto();
        skillDtoExpected.setId(1L);
        skillDtoExpected.setSkillName(skillDto.getSkillName());
        skillDtoExpected.setLevel(Expertise.NOT_APPLICABLE);

        when(service.update(1L, skillDto, null)).thenReturn(skillDtoExpected);

        MvcResult result = mvc.perform(put(API + V1 + "/skills/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(skillDtoExpected));
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnUpdateWithDefaultLevelValue_whenUpdateSkillDtoHasWrongValueProvided() throws Exception  {

        SkillIncomingDto skillDto = new SkillIncomingDto();
        skillDto.setSkillName("Java");
        String content = mapper.writeValueAsString(skillDto).replace(Expertise.NOT_APPLICABLE.name(), "unknown");
        skillDto.setLevel(Expertise.NOT_APPLICABLE);

        SkillDto skillDtoExpected = new SkillDto();
        skillDtoExpected.setId(1L);
        skillDtoExpected.setSkillName(skillDto.getSkillName());
        skillDtoExpected.setLevel(Expertise.NOT_APPLICABLE);

        when(service.update(1L, skillDto, null)).thenReturn(skillDtoExpected);

        MvcResult result = mvc.perform(put(API + V1 + "/skills/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(skillDtoExpected));
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnMethodNotAllowed_whenUpdateURIIsWrong() throws Exception  {

        SkillDto skillDto = new SkillDto();
        skillDto.setSkillName("Java");

        MvcResult result = mvc.perform(put(API + V1 + "/skills")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(skillDto)))
                .andExpect(status().isMethodNotAllowed())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":405");
        assertThat(response).contains("\"errors\":[\"Request method 'PUT' not supported\"]");
        assertThat(response).contains("\"type\":\"HttpRequestMethodNotSupportedException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/skills\"");
        assertThat(response).contains("\"message\":\"Method Not Allowed\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldUpdateContact_whenUpdateAndSkillDtoIsValid() throws Exception  {

        SkillIncomingDto incomingSkillDto = new SkillIncomingDto();
        incomingSkillDto.setLevel(Expertise.EXPERT);
        incomingSkillDto.setSkillName("Java");
        String content = mapper.writeValueAsString(incomingSkillDto).replace("EXPERT", "expert");

        SkillDto expectedSkillDto = new SkillDto();
        expectedSkillDto.setId(1);
        expectedSkillDto.setSkillName(incomingSkillDto.getSkillName());
        expectedSkillDto.setLevel(incomingSkillDto.getLevel());

        when(service.update(1L, incomingSkillDto, null)).thenReturn(expectedSkillDto);

        MvcResult result = mvc.perform(put(API + V1 + "/skills/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(expectedSkillDto));
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnNotFound_whenUpdateAndIdIsWrong() throws Exception  {

        SkillIncomingDto incomingDto = new SkillIncomingDto();
        incomingDto.setSkillName("Java");

        when(service.update(1L, incomingDto, null)).thenThrow(new SkillNotFoundException(1L));

        MvcResult result = mvc.perform(put(API + V1 + "/skills/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(incomingDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":404");
        assertThat(response).contains("\"errors\":[\"Skill not found id=1\"]");
        assertThat(response).contains("\"type\":\"SkillNotFoundException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/skills/1\"");
        assertThat(response).contains("\"message\":\"Not Found\"");
    }


    /* Testing delete() */
    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnNoContent_whenDeleteAndSkillFound() throws Exception {
        doNothing().when(service).delete(1L, null);

        mvc.perform(delete(API + V1 + "/skills/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnNotFound_whenDeleteAndSkillNotFound() throws Exception {
        doThrow(new SkillNotFoundException(1L)).when(service).delete(1L, null);

        MvcResult result = mvc.perform(delete(API + V1 + "/skills/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":404");
        assertThat(response).contains("\"errors\":[\"Skill not found id=1\"]");
        assertThat(response).contains("\"type\":\"SkillNotFoundException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/skills/1\"");
        assertThat(response).contains("\"message\":\"Not Found\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnMethodNotAllowed_whenDeleteURIIsWrong() throws Exception  {
        MvcResult result = mvc.perform(delete(API + V1 + "/skills"))
                .andExpect(status().isMethodNotAllowed())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":405");
        assertThat(response).contains("\"errors\":[\"Request method 'DELETE' not supported\"]");
        assertThat(response).contains("\"type\":\"HttpRequestMethodNotSupportedException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/skills\"");
        assertThat(response).contains("\"message\":\"Method Not Allowed\"");
    }
}