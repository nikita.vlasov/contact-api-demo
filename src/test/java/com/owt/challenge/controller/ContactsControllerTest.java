package com.owt.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.owt.challenge.dto.ContactDto;
import com.owt.challenge.dto.ContactIncomingDto;
import com.owt.challenge.entity.User;
import com.owt.challenge.exception.ContactNotFoundException;
import com.owt.challenge.service.ContactService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;

import static com.owt.challenge.paths.Paths.API;
import static com.owt.challenge.paths.Paths.V1;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ContactsController.class)
class ContactsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ContactService service;

    @Autowired
    private ObjectMapper mapper;

    /* Testing getAll() */
    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnEmptyList_whenGetAllNoContactsFound() throws Exception  {
        List<ContactDto> contacts = new ArrayList<>();
        when(service.findAll(null, null)).thenReturn(contacts);

        MvcResult result = mvc.perform(get(API + V1 + "/contacts")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(contacts));
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnListContacts_whenGetAndContactsFound() throws Exception  {

        List<ContactDto> contacts = Arrays.asList(
            new ContactDto(1L,"F1", "L1", "F1 L1", null, null, null, new HashSet<>()),
            new ContactDto(2L,"F2", "L2", "F2 L2", null, null, null, new HashSet<>()),
            new ContactDto(3L,"F3", "L3", "F3 L3", null, null, null, new HashSet<>())
                );

        when(service.findAll(null, null)).thenReturn(contacts);

        MvcResult result = mvc.perform(get(API + V1 + "/contacts")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(contacts));
    }


    /* Testing getById() */
    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnContact_whenGetAndIdIsCorrect() throws Exception  {

        ContactDto contact = new ContactDto();
        contact.setId(1L);
        contact.setFirstName("Name");
        contact.setLastName("LastName");
        contact.setFullName("Name LastName");

        when(service.findById(1L, null)).thenReturn(contact);

        MvcResult result = mvc.perform(get(API + V1 + "/contacts/1")
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic YWRtaW46YWRtaW4="))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(contact));
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnNotFound_whenGetAndIdIsWrong() throws Exception  {
        when(service.findById(1L,null)).thenThrow(new ContactNotFoundException(1L));

        MvcResult result = mvc.perform(get(API + V1 + "/contacts/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":404");
        assertThat(response).contains("\"errors\":[\"Contact not found id=1\"]");
        assertThat(response).contains("\"type\":\"ContactNotFoundException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/contacts/1\"");
        assertThat(response).contains("\"message\":\"Not Found\"");
    }


    /* Testing create() */
    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnBadRequest_whenCreateAndIncomingDtoNotValid() throws Exception  {

        ContactIncomingDto contact = new ContactIncomingDto();
        contact.setLastName("LastName");

        MvcResult result = mvc.perform(post(API + V1 + "/contacts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(contact)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":400");
        assertThat(response).contains("\"type\":\"MethodArgumentNotValidException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/contacts\"");
        assertThat(response).contains("\"message\":\"Invalid request\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnBadRequest_whenCreateAndIncomingDtoEmailNotValid() throws Exception  {

        ContactIncomingDto contact = new ContactIncomingDto();
        contact.setFirstName("FirstName");
        contact.setLastName("LastName");
        contact.setEmail("notValidEmail");

        MvcResult result = mvc.perform(post(API + V1 + "/contacts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(contact)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":400");
        assertThat(response).contains("\"errors\":[\"email: Email should be valid\"]");
        assertThat(response).contains("\"type\":\"MethodArgumentNotValidException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/contacts\"");
        assertThat(response).contains("\"message\":\"Invalid request\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnUnsupportedMediaType_whenCreateAndContentTypeHeaderIsNotJSON() throws Exception  {

        ContactIncomingDto contact = new ContactIncomingDto();
        contact.setLastName("LastName");

        mvc.perform(post(API + V1 + "/contacts")
                .content(mapper.writeValueAsString(contact)))
                .andExpect(status().isUnsupportedMediaType());
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnMethodNotAllowed_whenCreateURIIsWrong() throws Exception  {

        ContactIncomingDto contact = new ContactIncomingDto();
        contact.setLastName("LastName");

        MvcResult result = mvc.perform(post(API + V1 + "/contacts/1")
                .content(mapper.writeValueAsString(contact)))
                .andExpect(status().isMethodNotAllowed())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":405");
        assertThat(response).contains("\"errors\":[\"Request method 'POST' not supported\"]");
        assertThat(response).contains("\"type\":\"HttpRequestMethodNotSupportedException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/contacts/1\"");
        assertThat(response).contains("\"message\":\"Method Not Allowed\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldCreateContact_whenCreateAndContactIncomingDtoIsValid() throws Exception  {

        ContactIncomingDto incomingDto = new ContactIncomingDto();
        incomingDto.setFirstName("FirstName");
        incomingDto.setLastName("LastName");

        ContactDto contactDto = new ContactDto();
        contactDto.setId(1);
        contactDto.setFirstName(incomingDto.getFirstName());
        contactDto.setLastName(incomingDto.getLastName());
        contactDto.setFullName(incomingDto.getFirstName() + " " + incomingDto.getLastName());

        when(service.create(incomingDto,null)).thenReturn(contactDto);

        MvcResult result = mvc.perform(post(API + V1 + "/contacts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(incomingDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(contactDto));
    }


    /* Testing update() */
    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnUnsupportedMediaType_whenUpdateAndContentTypeHeaderIsNotJSON() throws Exception  {

        ContactIncomingDto contact = new ContactIncomingDto();
        contact.setLastName("LastName");

        mvc.perform(put(API + V1 + "/contacts/1")
                .content(mapper.writeValueAsString(contact)))
                .andExpect(status().isUnsupportedMediaType());
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnBadRequest_whenUpdateIncomingDtoNotValid() throws Exception  {

        ContactIncomingDto contact = new ContactIncomingDto();
        contact.setLastName("LastName");

        MvcResult result = mvc.perform(put(API + V1 + "/contacts/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(contact)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":400");
        assertThat(response).contains("\"type\":\"MethodArgumentNotValidException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/contacts/1\"");
        assertThat(response).contains("\"message\":\"Invalid request\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnBadRequest_whenUpdateAndContactIncomingDtoEmailNotValid() throws Exception  {

        ContactIncomingDto contact = new ContactIncomingDto();
        contact.setFirstName("FirstName");
        contact.setLastName("LastName");
        contact.setEmail("notValidEmail");

        MvcResult result = mvc.perform(put(API + V1 + "/contacts/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(contact)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":400");
        assertThat(response).contains("\"errors\":[\"email: Email should be valid\"]");
        assertThat(response).contains("\"type\":\"MethodArgumentNotValidException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/contacts/1\"");
        assertThat(response).contains("\"message\":\"Invalid request\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnMethodNotAllowed_whenUpdateURIIsWrong() throws Exception  {

        ContactIncomingDto contact = new ContactIncomingDto();
        contact.setLastName("LastName");

        MvcResult result = mvc.perform(put(API + V1 + "/contacts")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(contact)))
                .andExpect(status().isMethodNotAllowed())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":405");
        assertThat(response).contains("\"errors\":[\"Request method 'PUT' not supported\"]");
        assertThat(response).contains("\"type\":\"HttpRequestMethodNotSupportedException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/contacts\"");
        assertThat(response).contains("\"message\":\"Method Not Allowed\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldUpdateContact_whenUpdateAndContactIncomingDtoIsValid() throws Exception  {

        ContactIncomingDto incomingDto = new ContactIncomingDto();
        incomingDto.setFirstName("FirstName");
        incomingDto.setLastName("LastName");

        ContactDto contactDto = new ContactDto();
        contactDto.setId(1);
        contactDto.setFirstName(incomingDto.getFirstName());
        contactDto.setLastName(incomingDto.getLastName());
        contactDto.setFullName(incomingDto.getFirstName() + " " + incomingDto.getLastName());

        when(service.update(1L, incomingDto, null)).thenReturn(contactDto);

        MvcResult result = mvc.perform(put(API + V1 + "/contacts/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(incomingDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(mapper.writeValueAsString(contactDto));
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnNotFound_whenUpdateAndIdIsWrong() throws Exception  {

        ContactIncomingDto incomingDto = new ContactIncomingDto();
        incomingDto.setFirstName("FirstName");
        incomingDto.setLastName("LastName");

        when(service.update(1L, incomingDto, null)).thenThrow(new ContactNotFoundException(1L));

        MvcResult result = mvc.perform(put(API + V1 + "/contacts/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(incomingDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":404");
        assertThat(response).contains("\"errors\":[\"Contact not found id=1\"]");
        assertThat(response).contains("\"type\":\"ContactNotFoundException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/contacts/1\"");
        assertThat(response).contains("\"message\":\"Not Found\"");
    }


    /* Testing delete() */
    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnNoContent_whenDeleteAndContactFound() throws Exception {

        doNothing().when(service).delete(1L, null);

        mvc.perform(delete(API + V1 + "/contacts/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnNotFound_whenDeleteAndContactNotFound() throws Exception {

        doThrow(new ContactNotFoundException(1L)).when(service).delete(1L, null);

        MvcResult result = mvc.perform(delete(API + V1 + "/contacts/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":404");
        assertThat(response).contains("\"errors\":[\"Contact not found id=1\"]");
        assertThat(response).contains("\"type\":\"ContactNotFoundException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/contacts/1\"");
        assertThat(response).contains("\"message\":\"Not Found\"");
    }

    @Test
    @WithMockUser(username="admin", password = "admin", authorities = {"USER","ADMIN"})
    void shouldReturnMethodNotAllowed_whenDeleteURIIsWrong() throws Exception  {
        MvcResult result = mvc.perform(delete(API + V1 + "/contacts"))
                .andExpect(status().isMethodNotAllowed())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        assertThat(response).contains("timestamp");
        assertThat(response).contains("\"status\":405");
        assertThat(response).contains("\"errors\":[\"Request method 'DELETE' not supported\"]");
        assertThat(response).contains("\"type\":\"HttpRequestMethodNotSupportedException\"");
        assertThat(response).contains("\"path\":\"uri=/api/v1/contacts\"");
        assertThat(response).contains("\"message\":\"Method Not Allowed\"");
    }
}