package com.owt.challenge;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("liquibase-test")
class ChallengeApplicationTests {

	@Test
	void contextLoads() {
	}
}
